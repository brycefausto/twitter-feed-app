Twitter Feed App
================

Twitter Feed App uses the Twitter API to get the top trending/popular tweets and view the thread.

Functions:
----------

•	It allows users to discover the most trending/popular public tweets default by today but can be changed between this month, week and year.  
•	The tweets contains the user info of the one who posted the tweet i.e. name, profile image and also the information of the tweet i.e. likes, retweets, count of public comments, date posted etc.  
•	Tweets are sortable by an ascending or descending order and lazy loaded i.e. when the user reach the bottom part of the screen.  
•	The user also has an ability to search or filter out the trending tweets by inputting a name of a twitter account name.  
•	User can also view the tweets in per row or grid pattern.  
•	The user can click on a twitter user's name on the tweet then redirect the user to the twitter user's feed/profile or the list of tweets of the user who posted that tweet and could be sorted by popularity/trending and recent.  
•	The user could also view the public comments/reply along with the count of retweets, likes, date posted, user info etc., in short, the thread.  
•	There is another page for top public users either by popularity/followers or number of tweets with the public user's general info i.e. name number of tweets, followers etc. Also paginated and displayed as grid.  
•	When a user clicks on a twitter user in the top public users list, it will redirect the user to the twitter user's feed/profile.  

Authentication, as a user:
--------------------------

•	Register a new account using an email and password.  
•	Login a registered account using an email and password.  
•	The user cannot see the main twitter feed unless he/she logged-in a valid account.  

Requires:
---------

•	NodeJS v8.11.1 or above

Libraries/Frameworks:
---------------------

- Bootstrap
- react-redux
- react-router
- redux-form
- redux-act
- pouchDB
- axios
- lodash
- moment
- twit (for Twitter Api)
- etc.