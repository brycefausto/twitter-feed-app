const Twit = require('twit'),
    secretKey = require('./secret-key'),
    { readKeys } = require('./utils')

const T = new Twit(readKeys(secretKey))

module.exports = T