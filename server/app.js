const express = require('express'),
    routes = require('./routes'),
    path = require('path')

const app = express()

const port = process.env.PORT || 8080
const ip_address = '127.0.0.1'

app.use(express.static('dist'))

app.use('/api', routes)

app.get('*', (req, res) => {
    res.sendFile(path.resolve('dist', 'index.html'))
})

app.use((req, res) => {
    res.sendStatus(404)
})

app.use((err, req, res, next) => {
    console.log({ err })
    res.sendStatus(500)
})

app.listen(port, ip_address, () => console.log(`Listening on http://localhost:${port}`));
