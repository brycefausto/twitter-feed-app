const { Router } = require('express'),
    T = require('./twitter'),
    axios = require('axios'),
    { JSDOM } = require('jsdom'),
    moment = require('moment'),
    { decrInt64 } = require('./utils'),
    _ = require('lodash')

const router = Router()

async function getTrends(exclude) {
    let params = { id: 1 }
    if (exclude) {
        params.exclude = exclude
    }
    return (await T.get('trends/place', params)).data[0].trends
}

async function getPopularTweets(params) {
    return (await T.get('search/tweets', { ...params, result_type: 'popular', tweet_mode: 'extended' })).data.statuses
}

// get top 10 trends
async function getTweetsByTrends({ query, ...rest }) {
    let trends = await getTrends()
    trends = trends.slice(0, 10)
    let trendsQuery = trends.join(' OR ')
    let q = trendsQuery + query
    const tweets = await getPopularTweets({ q, count: 20, ...rest })
    return tweets
}

router.get('/', async (req, res, next) => {
    try {
        await axios.get('http://www.google.com', { timeout: 3000 })
    } catch (err) {
        next(err)
    }
    return
})

router.get('/search/tweets', async (req, res, next) => {
    let { q, since, until, ...rest } = req.query
    q = q || ''
    let tweets, query = ''
    since = since || moment().subtract(1, 'day').format('YYYY-MM-DD')
    query += ` since:${since}`
    if (until) {
        query += ` until:${until}`
    }
    try {
        if (!q) {
            tweets = await getTweetsByTrends({ query, ...rest })
        } else {
            q += query
            tweets = await getPopularTweets({ q, count: 20, ...rest })
        }
        res.json(tweets)
    } catch (err) {
        next(err)
    }
    return
})

router.get('/statuses/user_timeline', async (req, res, next) => {
    let { screen_name, max_id } = req.query
    let params = { screen_name, count: 20, tweet_mode: 'extended' }
    if (max_id) {
        params.max_id = max_id
    }
    try {
        const tweets = (await T.get('statuses/user_timeline', params)).data
        res.json(tweets)
    } catch (err) {
        next(err)
    }
    return
})

router.get('/users/show', async (req, res, next) => {
    const { screen_name } = req.query
    try {
        const data = (await T.get('users/show', { screen_name })).data
        res.send(data)
    } catch (err) {
        next(err)
    }
    return
})

router.get('/users/search', async (req, res, next) => {
    const { q } = req.query
    let data = []
    try {
        if (q) {
            data = (await T.get('users/search', { q, count: 6 })).data
        }
        res.send(data)
    } catch (err) {
        next(err)
    }
    return
})

router.get('/users/popular', async (req, res, next) => {
    try {
        const html = (await axios.get('https://friendorfollow.com/twitter/most-followers/')).data
        const { document } = new JSDOM(html).window
        const list = document.querySelectorAll("ul.view-list > li")
        let q = ""
        let i = 0
        list.forEach(li => {
            const tUser = li.querySelector("a.tUser")
            const screen_name = tUser.textContent
            q += screen_name.slice(1)
            i++
            if (i < 100) {
                q += ","
            }
        })
        let data = (await T.post('users/lookup', { screen_name: q })).data
        res.send(data)
    } catch (err) {
        next(err)
    }
    return
})

router.get('/statuses/show/:id', async (req, res, next) => {
    const { id } = req.params
    try {
        const { data } = await T.get(`statuses/show/${id}`, { tweet_mode: 'extended' })
        res.send(data)
    } catch (err) {
        next(err)
    }
    return
})

router.get('/statuses/replies', async (req, res, next) => {
    const { screen_name, tweet_id, tweet_date } = req.query
    try {
        let params = { q: `to:${screen_name}`, since_id: tweet_id, count: 100, tweet_mode: 'extended' }
        let { statuses } = (await T.get('search/tweets', params)).data
        let replies = _.filter(statuses, ['in_reply_to_status_id_str', tweet_id])
        let getMaxId = (statuses) => {
            const status = _.minBy(statuses, 'id_str')
            return status ? decrInt64(status.id_str) : ''
        }
        // get the hour difference from now
        let hourDiff = moment().diff(moment(tweet_date, "ddd MMM D HH:mm:ss ZZ YYYY").format(), 'hours')
        // set the maxCount of the loop to hourDiff not greater than 5
        let maxCount = hourDiff <= 5 ? hourDiff : 5
        let counter = 0
        let max_id = getMaxId(statuses)
        // request another 100 tweets per hour
        while ( counter < maxCount && max_id > tweet_id && replies.length < 10) {
            if (max_id) {
                params.max_id = max_id
                statuses = (await T.get('search/tweets', params)).data.statuses
                const newReplies = _.filter(statuses, ['in_reply_to_status_id_str', tweet_id])
                replies = _.unionBy(replies, newReplies, 'id')
                max_id = getMaxId(statuses)
                counter++
            }
        }
        res.send(replies)
    } catch (err) {
        next(err)
    }
    return
})

module.exports = router