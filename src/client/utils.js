function pad0 (numStr,len) {
    while (numStr.length < len) {
      numStr = "0" + numStr
    }
    return numStr
  }

export function decrInt64 (int64) {
  var result = ""
  var midpt = Math.floor(int64.length/2)
  var upper = int64.substring(0,midpt)
  var lower = int64.substring(midpt)
  var upperVal = parseInt(upper, 10)
  var lowerVal = parseInt(lower, 10)

  if (lowerVal === 0) {
    if (upperVal === 0) {
      // We don't support negative numbers
      result = "*ERROR*"
    }
    else {
      // borrow 1
      result = pad0((--upperVal).toString(),upper.length) +
                (parseInt("1" + lower, 10) - 1).toString()
    }
  }
  else {
    var newLower = (lowerVal - 1).toString()
    result = upper + pad0(newLower,lower.length)
  }
  return result
}

export function nFormatter(num) {
  if (num >= 1000000000) {
     return (num / 1000000000).toFixed(1).replace(/\.0$/, '') + 'G'
  }
  if (num >= 1000000) {
     return (num / 1000000).toFixed(1).replace(/\.0$/, '') + 'M'
  }
  if (num >= 1000) {
     return (num / 1000).toFixed(1).replace(/\.0$/, '') + 'K'
  }
  return num
}