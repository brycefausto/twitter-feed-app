import { DB } from './db'
import moment from 'moment'
import { isOnline } from '../globals'

class CacheDB extends DB {
    constructor() {
        super('cache')
    }

    setDuration(amount, unit) {
        return moment().add(amount, unit).format()
    }

    async putCache(key, data, expires_at) {
        if (data && data.length) {
            await this.db.put({ _id: key, data, expires_at })
        }
    }

    async getCache(key) {
        const { db } = this
        const doc = await db.get(key)
        const now = moment()
        // if the cache is expired and is online, remove all the expired cache and throw error
        if (moment().isSameOrAfter(moment(doc.expires_at)) && isOnline()) {
            await db.remove(doc)
            const { docs } = await db.find({ selector: { expires_at: { $lte: now.format() } } })
            if (docs.length) {
                docs.forEach(doc => {
                    doc._deleted = true
                })
                await db.bulkDocs(docs)
            }
            throw new Error()
        }
        return doc.data
    }

}

export const cacheDB = new CacheDB()
