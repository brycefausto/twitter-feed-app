import PouchDB from 'pouchdb-browser'
import PouchDBFind from 'pouchdb-find'
PouchDB.plugin(PouchDBFind)

export class DB {
    constructor(name) {
        this.db = new PouchDB(name)
    }

    close() {
        this.db.close()
    }
}
