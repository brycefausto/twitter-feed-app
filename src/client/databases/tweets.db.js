import { DB } from './db'
import { tweetUsersDB } from './tweet-users.db'
import moment from 'moment'

class TweetsDB extends DB {
    constructor() {
        super('tweets')
    }

    // Store tweets with additional attributes
    async storeTweets(tweets, attrs) {
        for (let tweet of tweets) {
            try {
                await this.db.get(tweet.id_str)
            } catch (err) {
                tweet = { ...tweet, ...attrs, _id: tweet.id_str }
                const date = moment(tweet.created_at, "ddd MMM D HH:mm:ss ZZ YYYY").format()
                tweet.date_created = date
                await this.db.put(tweet)
                let { user } = tweet
                user = { ...user, _id: user.id_str }
                await tweetUsersDB.putUser(user)
            }
        }
        return
    }

    async getTweets(query) {
        let { max_id, since, ...selector } = query
        selector = selector || { _id: { $gt: null } }
        if (max_id) {
            selector._id = { $lte: max_id }
        }
        if (since) {
            since = moment(since, 'YYYY-MM-DD').format()
            selector.date_created = { $gte: since }
        }
        let { docs } = await this.db.find({ selector, limit: 20, sort: [{ _id: 'desc' }] })
        return docs
    }

    async getPopularTweets(query) {
        query = query || {}
        query.type = 'popular'
        return await this.getTweets(query)
    }

    async getTweetsByUser(screen_name, query) {
        query = query || {}
        query['user.screen_name'] = screen_name
        return await this.getTweets(query)
    }

}

export const tweetsDB = new TweetsDB()

