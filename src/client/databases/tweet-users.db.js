import { DB } from './db'

class TweetUsersDB extends DB {
    constructor() {
        super('tweet_users')
    }

    async putUser(user, attr) {
        try {
            await this.db.get(user._id)
        } catch (err) {
            user._id = user.id_str
            user = { ...user, ...attr }
            await this.db.put(user)
        }
        return user
    }

    async storeUsers(users, attr) {
        for (let i = 0; i < users.length; i++) {
            users[i] = await this.putUser(users[i], attr)
        }
        return users
    }

    async searchUsers(q) {
        const { docs } = await this.db.find({ selector: { $or: [
            { name: { $regex: q } },
            { screen_name: { $regex: q } }
        ] }, limit: 6 })
        return docs
    }

    async getUser(screen_name) {
        const { docs } = await this.db.find({ selector: { screen_name } })
        if (!docs) {
            return
        }
        return docs[0]
    }

    async getPopularUsers() {
        const { docs } = await this.db.find({ selector: { type: 'popular' } })
        if (!docs) {
            return
        }
        return docs
    }
}

export const tweetUsersDB = new TweetUsersDB()
