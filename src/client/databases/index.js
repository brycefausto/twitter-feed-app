import { usersDB } from './user.db'
import { tweetsDB } from './tweets.db'
import { tweetUsersDB } from './tweet-users.db'
import { cacheDB } from './cache.db'

export { usersDB, tweetsDB, tweetUsersDB, cacheDB }

export function closeDBs() {
    usersDB.close()
    tweetsDB.close()
    tweetUsersDB.close()
    cacheDB.close()
}