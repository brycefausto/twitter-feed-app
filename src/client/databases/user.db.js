import { DB } from './db'
import bcrypt from 'bcryptjs'
import uuidv1 from 'uuid/v1'

class UsersDB extends DB {

    constructor() {
        super('users')
    }

    async getUser(id) {
        const user = await this.db.get(id)
        return user
    }

    async createUser(email, password) {
        password = bcrypt.hashSync(password)
        const { id } = await this.db.put({
            _id: uuidv1(),
            email,
            password
        })
        const user = await this.getUser(id)
        return user
    }

    async findUser(email, password) {
        const user = (await this.db.find({ selector: { email }, limit: 1 })).docs[0]
        if (user) {
            if (!bcrypt.compareSync(password, user.password)) {
                throw new Error('The password is invalid')
            }
            return user
        } else {
            throw new Error('User not found')
        }
    }

    async updateUser(id, email, password) {
        let user = await this.getUser(id)
        password = password ? bcrypt.hashSync(password) : user.password
        user = {
            ...user,
            email,
            password
        }
        const { ok } = await this.db.put(user)
        if (ok) {
            return user
        }
        return
    }

}

export const usersDB = new UsersDB()
