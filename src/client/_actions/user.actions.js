import { createAction } from 'redux-act'
import { usersDB } from '../databases'
import { alertSuccess, alertError } from './alert.actions'

const KEY_USER_ID = 'userId'

export const setUser = createAction('SET_USER')
export const clearUser = createAction('CLEAR_USER')

export function signUpUser(email, password) {
    return async dispatch => {
        try {
            const user = await usersDB.createUser(email, password)
            localStorage.setItem(KEY_USER_ID, user._id)
            setTimeout(() => dispatch(alertSuccess('You successfully signed in!')), 500)
            return dispatch(setUser(user))
        } catch (err) {
            dispatch(alertError(err))
            throw err
        }
    }
}

export function loginUser(email, password) {
    return async dispatch => {
        try {
            const user = await usersDB.findUser(email, password)
            localStorage.setItem(KEY_USER_ID, user._id)
            return dispatch(setUser(user))
        } catch (err) {
            dispatch(alertError(err))
            throw err
        }
    }
}

export function logoutUser() {
    return async dispatch => {
        localStorage.removeItem(KEY_USER_ID)
        return dispatch(clearUser())
    }
}

export function checkUser() {
    return async dispatch => {
        const id = localStorage.getItem(KEY_USER_ID)
        if (id) {
            try {
                const user = await usersDB.getUser(id)
                if (!user) {
                    localStorage.removeItem(KEY_USER_ID)
                    return dispatch(alertError('User not found!'))
                }
                return dispatch(setUser(user)) 
            } catch (err) {
                localStorage.removeItem(KEY_USER_ID)
                dispatch(alertError(err))
                throw err
            }
        }
    }
}

export function updateUser({ id, email, password }) {
    return async dispatch => {
        const updatedUser = await usersDB.updateUser(id, email, password)
        return dispatch(setUser(updatedUser))
    }
}
