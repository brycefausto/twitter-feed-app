import { createAction } from 'redux-act'

export const alertSuccess = createAction('ALERT_SUCCESS')
export const alertWarning = createAction('ALERT_WARNING')
export const alertError = createAction('ALERT_ERROR', error => {
    if (typeof error === 'string') {
        return error
    } else {
        let { name, message } = error
        if (name !== 'Error') {
            message = `${name}: ${message}`
        }
        return message
    }
})
export const clearAlert = createAction('CLEAR_ALERT')
