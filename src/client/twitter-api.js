import axios from 'axios'
import moment from 'moment'
import { tweetsDB, tweetUsersDB, cacheDB } from './databases'
import { setHasInternet } from './globals'
async function get(url, params) {
    try {
        const { data } = await axios.get('/api' + url, { params })
        setHasInternet(true)
        return data
    } catch (err) {
        // TODO check error if 404
        // if error is 404 {
            setHasInternet(false)
        // }
        throw err
    }
}

function getURL(path, params) {
    const urlParams = Object.entries(params).map(e => e.join('=')).join('&')
    return `${path}?${urlParams}`
}

class TwitterApi {

    // fetch data and store to redux
    async fetchPopularTweets(query) {
        const path = '/search/tweets'
        const key = getURL(path, query)
        try {
            let data = []
            try {
                data = await cacheDB.getCache(key)
            } catch (err1) {
                data = await get(path, query)
                tweetsDB.storeTweets(data, { type: 'popular' })
                cacheDB.putCache(key, data, cacheDB.setDuration(1, 'hour'))
            }
            return data
        } catch (err) {
            return await tweetsDB.getPopularTweets(query)
        }
    }

    async fetchTweetsByUser(screen_name, query) {
        const path = '/statuses/user_timeline'
        const params = { screen_name, ...query}
        const key = getURL(path, params)
        try {
            let data = []
            try {
                data = await cacheDB.getCache(key)
            } catch (err1) {
                data = await get(path, params)
                tweetsDB.storeTweets(data)
                cacheDB.putCache(key, data, cacheDB.setDuration(1, 'hour'))  
            }
            return data
        } catch (err) {
            return await tweetsDB.getTweetsByUser(screen_name, query)
        }
    }

    async searchUsers(q) {
        let data = []
        try {
            const path = '/users/search'
            const key = getURL(path, q)
            try {
                data = await cacheDB.getCache(key)
            } catch (err1) {
                data = await get(path, { q })
                tweetUsersDB.storeUsers(data)
                cacheDB.putCache(key, data, cacheDB.setDuration(1, 'day'))
            }
        } catch (err) {
            data = tweetUsersDB.searchUsers(q)
        }
        return data
    }

    async fetchUser(screen_name) {
        let user
        try {
            user = await tweetUsersDB.getUser(screen_name)
            if (!user) {
                throw new Error()
            } else {
                return user
            }
        } catch (err) {
            user = await get('/users/show', { screen_name })
            tweetUsersDB.putUser(user)
            return user
        }
    }

    async fetchPopularUsers() {
        let users = []
        try {
            const path = '/users/popular'
            try {
                users = await cacheDB.getCache(path)
            } catch (err1) {
                users = await get(path)
                tweetUsersDB.storeUsers(users, { type: 'popular' })
                cacheDB.putCache(path, users, cacheDB.setDuration(1, 'day'))
            }
        } catch (err) {
            users = await tweetUsersDB.getPopularUsers()
        }
        return users
    }

    async fetchTweet(id) {
        const path = `/statuses/show/${id}`
        let tweet = {}
        try {
            tweet = await cacheDB.getCache(path)
        } catch (err) {
            tweet = await get(path)
            cacheDB.putCache(path, tweet, cacheDB.setDuration(1, 'week'))
        }
        return tweet
    }

    async fetchTweetReplies(screen_name, tweet_id, tweet_date) {
        const path = '/statuses/replies'
        const params = { screen_name, tweet_id, tweet_date }
        const key = getURL(path, params)
        let data = []
        try {
            data = await cacheDB.getCache(key)
        } catch (err) {
            data = await get(path, params)
            cacheDB.putCache(key, data, cacheDB.setDuration(5, 'minutes'))
        }
        return data
    }

}

const twitterApi = new TwitterApi()

export default twitterApi

export function parseDate(twitterDate) {
    const date = moment(twitterDate, "ddd MMM D HH:mm:ss ZZ YYYY")
    return date
}

export function formatDate(twitterDate, dateFormat) {
    const date = parseDate(twitterDate)
    return date.format(dateFormat)
}
