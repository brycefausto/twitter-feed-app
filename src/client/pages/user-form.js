import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import { Link } from 'react-router-dom'
import { loginUser, signUpUser, logoutUser } from '../_actions'
import { Input } from '../components'

let UserForm = props => {
    const { handleSubmit, login } = props
    return (
        <form onSubmit={handleSubmit}>
            <Field name="email" type="email" label="Email" component={Input} />
            <Field name="password" type="password" label="Password" component={Input} />
            <div className="buttons">
                <button className="btn btn-primary">Submit</button>
                {!login && <Link to="/login" className="btn btn-secondary">Cancel</Link>}
            </div>
            {login &&
                <div className="mt-2">
                    Don't have account yet? <Link to="/signup" className="text-primary">Sign up now »</Link>
                </div>
            }
        </form>
    )
}

UserForm = reduxForm({ form: 'user' })(UserForm)

class UserFormPage extends Component {
    constructor(props) {
        super(props)
        const { logout, logoutUser, history } = props
        if (logout) {
            logoutUser()
            history.replace('/login')
        }
    }
    onSubmit = async values => {
        const { email, password } = values
        const { login, loginUser, signUpUser, history } = this.props
        try {
            if (login) {
                await loginUser(email, password)
            } else {
                await signUpUser(email, password)
            }
            history.replace('/')
        } catch (err) {
        }
    }
    render() {
        const { login } = this.props
        return (
            <div className="mx-auto w-25">
                <h1>{ login ? 'Login' : 'Sign Up' }</h1>
                <UserForm login={login} onSubmit={this.onSubmit} />
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => (bindActionCreators({ loginUser, signUpUser, logoutUser }, dispatch))

export default connect(null, mapDispatchToProps)(UserFormPage)
