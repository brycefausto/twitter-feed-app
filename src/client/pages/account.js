import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import { setUser } from '../_actions'
import { Input } from '../components'

class EditForm extends Component {
    state = {
        changePassword: false
    }
    handleCheckbox = ({ target: { checked } }) => {
        this.setState({ changePassword: checked })
    }
    render() {
        const { handleSubmit } = this.props
        const { changePassword } = this.state
        return (
            <form onSubmit={handleSubmit}>
                <Field name="email" type="email" label="Email" component={Input} />
                <div className="form-group form-check">
                    <input type="checkbox" value={changePassword} checked={changePassword} onChange={this.handleCheckbox} className="form-check-input" id="changePassword" />
                    <label className="form-check-label" htmlFor="changePassword">Change Password</label>
                </div>
                {changePassword &&
                    <Field name="password" type="password" label="Password" component={Input} />
                }
                <button type="submit" className="btn btn-primary mr-5">Save changes</button>
                <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
            </form>
        )        
    }
}

class AccountPage extends Component {
    onSubmit = async values => {
        console.log({ values })
        const { email, password } = values
        this.props.dispatch(setUser({ email, password }))
    }
    render() {
        const { user } = this.props
        let EditAccountForm = reduxForm({ form: 'edit', initialValues: { email: user.email } })(EditForm)
        return (
            <div>
                <h1>My Account</h1>
                <div className="card card-body w-25">
                    <p><strong>Email: </strong>{user.email}</p>
                    <hr/>
                    <button className="btn btn-success" data-toggle="modal" data-target="#editModal">Edit</button>
                </div>
                <div className="modal fade" id="editModal" tabIndex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="modalLabel">Edit Account</h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        <EditAccountForm onSubmit={this.onSubmit} />
                    </div>
                    </div>
                </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({ user: state.user })

export default connect(mapStateToProps)(AccountPage)
