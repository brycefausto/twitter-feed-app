import React, { Component } from 'react'
import twitterApi from '../twitter-api'
import { TweetThread, TweetContent } from '../components'

class HomePage extends Component {
    async fetchTweets(query) {
        return await twitterApi.fetchPopularTweets(query)
    }
    render() {
        return (
            <div className="mx-auto">
                <h1>Popular Tweets</h1>
                <TweetThread />
                <TweetContent fetchTweets={this.fetchTweets} />
            </div>
        )
    }
}

export default HomePage
