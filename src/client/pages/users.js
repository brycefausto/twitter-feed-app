import React, { Component } from 'react'
import { UsersContent } from '../components'
import qs from 'query-string'

class TopUsersPage extends Component {
    render() {
        const query = qs.parse(this.props.location.search)
        return (
            <div className="mx-auto">
                <h1>Top Users</h1>
                <UsersContent page={query.page} />
            </div>
        )
    }
}

export default TopUsersPage
