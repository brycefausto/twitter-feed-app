import React, { Component } from 'react'
import { connect } from 'react-redux'
import twitterApi from '../twitter-api'
import { TweetThread, TweetContent, Loading } from '../components'
import { alertError } from '../_actions'
import { nFormatter } from '../utils'

let mounted = false


class UserFeedsPage extends Component {
    state = {
        loading: true,
        user: {}
    }
    async componentDidMount() {
        mounted = true
        await this.fetchUser(this.props)
    }
    componentWillUnmount() {
        mounted = false
    }
    async componentWillReceiveProps(nextProps) {
        this.setState({ loading: true })
        await this.fetchUser(nextProps)
    }
    fetchUser = async (props) => {
        let user = {}
        const { match: { params: { screen_name } }, dispatch } = props
        try {
            user = await twitterApi.fetchUser(screen_name)
            user.profile_image_url = user.profile_image_url.replace('_normal', '_400x400')
        } catch (err) {
            dispatch(alertError(err))
        }
        if (mounted) {
            this.setState({ loading: false, user })
        }
        return
    }
    fetchTweets = async query => {
        const { screen_name } = this.props.match.params
        return await twitterApi.fetchTweetsByUser(screen_name, query)
    }
    LoadingUser = () => {
        return (
            <div className="mx-auto">
                <Loading />
            </div>
        )
    }
    Content = () => {
        const { user } = this.state
        return (
            <div className="mx-auto">
                <div className="card">
                    <div className="tweet-profile-header">
                        <div className="tweet-profile-header-bg">
                            <img className="card-img-top" src={user.profile_banner_url} alt=""/>
                        </div>
                        <div className="tweet-profile-avatar-container">
                            <img className="tweet-profile-avatar-lg rounded-circle" src={user.profile_image_url} alt="" />
                        </div>
                    </div>
                    <div className="card-body">
                        <div className="tweet-profile-name">
                            <span className="tweet-profile-fullname">{user.name}</span>
                            <span className="tweet-username"><s>@</s><b>{user.screen_name}</b></span>
                        </div>
                    </div>
                    <div className="card-footer bg-white text-muted">
                        <div className="user-stats-list">
                            <div className="user-stats-item">
                                <span className="user-stats-label">Tweets</span>
                                <span className="user-stats">{nFormatter(user.statuses_count)}</span>
                            </div>
                            <div className="user-stats-item">
                                <span className="user-stats-label">Following</span>
                                <span className="user-stats">{nFormatter(user.friends_count)}</span>
                            </div>
                            <div className="user-stats-item">
                                <span className="user-stats-label">Followers</span>
                                <span className="user-stats">{nFormatter(user.followers_count)}</span>
                            </div>
                            <div className="user-stats-item">
                                <span className="user-stats-label">Likes</span>
                                <span className="user-stats">{nFormatter(user.favourites_count)}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <TweetThread />
                <TweetContent fetchTweets={this.fetchTweets} forUserFeeds={true} />
            </div>
        )
    }
    render() {
        const { loading } = this.state
        return (
            loading
            ? <this.LoadingUser />
            : <this.Content />
        )
    }
}

export default connect()(UserFeedsPage)
