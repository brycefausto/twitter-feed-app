import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import { connect } from 'react-redux'
import './App.css'
import { checkUser } from './_actions'

import { HomeContent, Navbar, Alert, PrivateRoute, Loading } from'./components'
import { HomePage, UsersPage, UserFormPage, AccountPage, UserFeedsPage, NotFoundPage } from './pages'

import { closeDBs } from './databases'

class App extends Component {

  state = {
    loading: true,
    isAuth: false
  }

  async componentDidMount() {
    const { dispatch } = this.props
    await dispatch(checkUser())
    this.setState({ loading: false })
  }

  componentWillUnmount() {
    closeDBs()
  }

  render() {
    const { user } = this.props
    if (this.state.loading) {
      return <Loading />
    }
    return (
      <Router>
        <div>
          {user.isAuth &&
            <Navbar user={user} />
          }
          <Alert/>
          {!user.isAuth &&
            <HomeContent />
          }
          <div className="container main">
            <Switch>
              <PrivateRoute exact path="/" component={HomePage} />
              <PrivateRoute exact path="/top_users" component={UsersPage} />
              <Route exact path="/signup" component={UserFormPage} />
              <Route exact path="/logout" component={props => (<UserFormPage {...props} logout={true} />)} />
              <Route exact path="/login" component={props => (<UserFormPage {...props} login={true} />)} />
              <Route exact path="/account" component={AccountPage} />
              <Route exact path="/user/:screen_name" component={UserFeedsPage} />
              <Route component={NotFoundPage} />
            </Switch>
          </div>
        </div>
      </Router>
    )
  }
}

const mapStateToProps = state => ({ user: state.user })

export default connect(mapStateToProps)(App)
