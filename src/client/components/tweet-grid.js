import React from 'react'
import { Tweet } from '../components'

const TweetGrid = ({ tweets }) => (
    <div className="tweet-grid">
        <div className="card-columns tweet-columns">
            {tweets.map(tweet => (
                <div className="card border-0" key={tweet.id} >
                    <Tweet data={tweet} />
                </div>
            ))}
        </div>
    </div>
)
export default TweetGrid