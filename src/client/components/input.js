import React from 'react'

const Input = ({ input, label, type, meta: { error } }) => {
    return (
        <div className="form-group">
            <input  {...input} type={type} placeholder={label} className="form-control" />
            <small className="invalid-feedback">{error}</small>
        </div>
    )
}

export default Input