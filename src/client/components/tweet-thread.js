import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Loading, Tweet } from '../components'
import twitterApi from '../twitter-api'
import { alertError } from '../_actions'
import $ from 'jquery';

class TweetThread extends Component {
    state = {
        loading: true,
        loadingReplies: false,
        tweet: {},
        replies: []
    }
    async componentDidMount() {
        $('#tweetThread').on('show.bs.modal', event => {
            const tweet_id = $(event.relatedTarget).data('tweet-id')
            this.fetchUser(tweet_id)
        })
    }
    fetchUser = async tweet_id => {
        const { dispatch } = this.props
        if (tweet_id) {
            try {
                this.setState({ loading: true })
                const tweet = await twitterApi.fetchTweet(tweet_id)
                this.setState({ loading: false, loadingReplies: true, tweet })
                const replies = await twitterApi.fetchTweetReplies(tweet.user.screen_name, tweet_id, tweet.created_at)
                this.setState({ loadingReplies: false, replies })
            } catch (err) {
                dispatch(alertError(err))
                $('#tweetThread').modal('hide')
            }
        }
    }
    render() {
        const { loading, loadingReplies, tweet, replies } = this.state
        const Content = () => (
            <div>
                <Tweet data={tweet} />
                {loadingReplies
                ? <Loading />
                : (
                    replies.length > 0
                    ? replies.map(reply => (
                        <Tweet key={reply.id} data={reply} />
                    ))
                    : <div className="mt-2 mb-2"><span>No comments</span></div>
                )
                }
            </div>
        )
        return (
            <div className="modal fade" id="tweetThread" tabIndex="-1" role="dialog" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-body">
                            {loading
                            ? <Loading />
                            : <Content />
                        }
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default connect()(TweetThread);
