import React from 'react'
import { Tweet } from '../components'

const TweetList = ({ tweets }) => (
    <div className="tweet-list">
        {tweets.map(tweet => (
            <Tweet data={tweet} key={tweet.id} />
        ))}
    </div>
)
export default TweetList