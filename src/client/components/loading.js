import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const Loading = () => (
    <div className="text-center">
      <h1 className="display-4 mt-5">
        <FontAwesomeIcon icon="spinner" spin className="mr-1" />
        Loading...
      </h1>
    </div>
)

export default Loading