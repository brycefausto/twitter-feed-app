import React, { Component } from 'react'
import { Link, NavLink, withRouter } from 'react-router-dom'

export function paginate(page, items, itemsPerPage, pagesLimit) {
    page = parseInt(page, 10) || 1
    const { length } = items
    const count = Math.ceil(length / itemsPerPage)
    let start = page
    const bound = count - pagesLimit
    if (start > bound) {
        start = bound
    }
    const end = start + pagesLimit
    let pages = []
    for (let i = start; i < end; i++) {
        pages.push(i)
    }
    const prevPage = page - 1
    let nextPage = page + 1
    if (nextPage > count) {
        nextPage = 0
    }
    const skip = (page - 1) * itemsPerPage
    if (skip < length) {
        items = items.slice(skip, skip + itemsPerPage)
    }
    return { page, pages, prevPage, nextPage, items, count }
}

class Pagination extends Component {
    getLink = page => {
        const { pathname } = this.props.location
        return `${pathname}?page=${page}`
    }
    render() {
        const { page, pages, prevPage, nextPage, count } = this.props
        const { getLink } = this
        return (
            <nav aria-label="pagination">
            {count > 1 &&
                <div className="pagination">
                    {page !== 1 &&
                        <Link className="page-item page-link" to={getLink(1)} aria-label="Previous">
                            <span aria-hidden="true">&lt;&lt;</span>
                            <span className="sr-only">First</span>
                        </Link>
                    }
                    {prevPage > 0 &&
                        <Link className="page-item page-link" to={getLink(prevPage)} aria-label="Previous">
                            <span aria-hidden="true">&lt;</span>
                            <span className="sr-only">Previous</span>
                        </Link>
                    }
                    {pages.map(page => (
                        <NavLink className="page-item page-link" to={getLink(page)} activeClassName="active" key={page}>{page}</NavLink>
                    ))}
                    {nextPage > 0 &&
                        <Link className="page-item page-link" to={getLink(nextPage)} aria-label="Next">
                            <span aria-hidden="true">&gt;</span>
                            <span className="sr-only">Next</span>
                        </Link>
                    }
                    {page !== count &&
                        <Link className="page-item page-link" to={getLink(count)} aria-label="Next">
                            <span aria-hidden="true">&gt;&gt;</span>
                            <span className="sr-only">Last</span>
                        </Link>
                    }
                </div>
            }
            </nav>
        )
    }
}

export default withRouter(Pagination)
