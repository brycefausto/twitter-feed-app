import React from 'react'

const HomeContent = () => (
    <div className="jumbotron jumbotron-fluid">
        <div className="container">
            <h1 className="display-4">Twitter Feed App</h1>
            <p className="lead">It uses the Twitter API to get the top trending/popular tweets and view the thread. </p>
        </div>
    </div>
)

export default HomeContent
