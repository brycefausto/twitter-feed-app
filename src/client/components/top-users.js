import React from 'react'
import { Link } from 'react-router-dom'
import { nFormatter } from '../utils' 

const UserItem = ({ user }) => (
    <div className="card user-card">
    <div className="card-body">
        <div className="tweet-profile-container">
            <Link to={`user/${user.screen_name}`} className="link">
                <img className="tweet-profile-avatar rounded-circle" src={user.profile_image_url} alt="" />
                <span className="tweet-fullname">{user.name}</span>
                <span className="tweet-username"><s>@</s><b>{user.screen_name}</b></span>
            </Link>
        </div>
    </div>
    <div className="card-footer bg-white text-muted">
        <div className="user-stats-list">
            <div className="user-stats-item">
                <span className="user-stats-label">Tweets</span>
                <span className="user-stats">{nFormatter(user.statuses_count)}</span>
            </div>
            <div className="user-stats-item">
                <span className="user-stats-label">Following</span>
                <span className="user-stats">{nFormatter(user.friends_count)}</span>
            </div>
            <div className="user-stats-item">
                <span className="user-stats-label">Followers</span>
                <span className="user-stats">{nFormatter(user.followers_count)}</span>
            </div>
        </div>
    </div>
</div>
)

const TopUsers = ({ users }) => (
    <div className="tweet-grid">
        <div className="row">
            {users.map(user => (
                <div className="col mb-1" key={user.id} >
                    <UserItem user={user} />
                </div>
            ))}
        </div>
    </div>
)
export default TopUsers 