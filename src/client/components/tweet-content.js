import React, { Component } from 'react'
import { connect } from 'react-redux'
import { TweetList, TweetGrid, Sidebar, Loading } from './'
import { alertError } from '../_actions'
import { decrInt64 } from '../utils'
import moment from 'moment'
import _ from 'lodash'

let mounted = false

const KEYS = {
    DATE: 'setting_date',
    ORDER: 'setting_order',
    TYPE: 'setting_type',
    VIEW: 'setting_view'
}

function getSetting(key, defaultValue) {
    let value = localStorage.getItem(key)
    if (!value) {
        value = defaultValue
        localStorage.setItem(key, value)
    }
    return value
}

function getExactDate(dateSetting) {
    const dateFormat = 'YYYY-MM-DD'
    switch (dateSetting) {
        case 'today':
            return moment().subtract(1, 'day').format(dateFormat)
        case 'week':
            return moment().startOf('week').format(dateFormat)
        case 'month':
            return moment().startOf('month').format(dateFormat)
        case 'year':
            return moment().startOf('year').format(dateFormat)
        default:
            return
    }
}

class TweetContent extends Component {
    state = {
        loading: true,
        loadingNext: false,
        hasNext: true,
        date: 'today',
        order: 'desc',
        type: 'recent',
        view: 'rows',
        tweets: [],
        max_id: ''
    }
    setStateAsync = async state => {
        this.setState(state, () => {
            return
        })
    }
    async componentDidMount() {
        mounted = true
        const { dispatch } = this.props
        let { date, order, type, view } = this.state
        date = getSetting(KEYS.DATE, date)
        order = getSetting(KEYS.ORDER, order)
        type = getSetting(KEYS.TYPE, type)
        view = getSetting(KEYS.VIEW, view)
        await this.setStateAsync({ date, order, type, view })
        let tweets = []
        try {
            tweets = await this.fetchTweets()
        } catch (err) {
            dispatch(alertError(err))
        }
        if (mounted) {
            this.setState({ loading: false, tweets })
        }
        window.addEventListener('scroll', this.onScroll)
    }
    componentWillUnmount() {
        mounted = false
        window.removeEventListener('scroll', this.onScroll)
    }
    fetchTweets = async () => {
        let{ max_id, date, order, type } = this.state
        let query = { since: getExactDate(date) }
        if (max_id) {
            query.max_id = max_id
        }
        let tweets = await this.props.fetchTweets(query)
        tweets = this.sortTweets(tweets, order, type)
        const lastTweet = _.minBy(tweets, 'id_str')
        max_id = lastTweet ? decrInt64(lastTweet.id_str) : this.state.max_id
        this.state.max_id = max_id
        return tweets
    }
    sortTweets = (tweets, order, type) => {
        if (type === 'recent') {
            tweets = _.orderBy(tweets, ['id'], [order])
        } else if (type === 'popularity') {
            tweets.sort((a, b) => {
                const a_counts = a.retweet_count + a.favorite_count
                const b_counts = b.retweet_count + b.favorite_count
                if (order === 'desc') {
                    return b_counts - a_counts
                } else {
                    return a_counts - b_counts
                }
            })
        }
        return tweets;
    }
    onChangeDate = async date => {
        await this.setStateAsync({ date, max_id: '', hasNext: true, loading: true })
        localStorage.setItem(KEYS.DATE, date)
        let tweets = await this.fetchTweets()
        this.setState({ tweets, loading: false })
    }
    onChangeOrder = order => {
        let { tweets } = this.state
        this.setState({ order, tweets: tweets.reverse() })
        localStorage.setItem(KEYS.ORDER, order)
    }
    onChangeType = type => {
        let { tweets, order } = this.state
        tweets = this.sortTweets(tweets, order, type)
        this.setState({ type, tweets })
    }
    onChangeView = view => {
        this.setState({ view })
        localStorage.setItem(KEYS.VIEW, view)
    }
    onScroll = async () => {
        const { innerHeight, scrollY } = window
        const sum = innerHeight + scrollY
        const offsetHeight = document.body.offsetHeight
        let { loadingNext, hasNext, order, type } = this.state
        if (sum >= offsetHeight && !loadingNext && hasNext) {
            this.setState({ loadingNext: true })
            let tweets = await this.fetchTweets()
            if (!tweets || !tweets.length) {
                if (mounted) {
                    this.setState({ loadingNext: false, hasNext: false })
                }
            } else {
                const prevTweets = this.state.tweets
                tweets = _.unionBy(prevTweets, tweets, 'id')
                tweets = this.sortTweets(tweets, order, type)
                if (mounted) {
                    this.setState({ loadingNext: false, hasNext: true, tweets }, () => {
                        if (order === 'asc') {
                            window.scrollTo(0, 400)
                        }
                    })
                }
            }
        }
    }
    render() {
        const { forUserFeeds } = this.props
        let { loading, loadingNext, tweets, date, order, type, view } = this.state
        const settings = { date, order, type, view }
        tweets = tweets || []
        return (
            <div className="row mt-2 mb-2">
                {loading
                    ? <div className="col"><Loading /></div>
                    : <div className="col">
                        {view !== 'grid'
                        ? <TweetList tweets={tweets} />
                        : <TweetGrid tweets={tweets} />
                        }
                        {loadingNext &&
                            <Loading />
                        }
                    </div>
                    }
                <div className="col-2 sidebar">
                    <div className="sticky-top sticky-offset">
                        <Sidebar
                            {...settings}
                            forUserFeeds={forUserFeeds}
                            onChangeDate={this.onChangeDate}
                            onChangeOrder={this.onChangeOrder}
                            onChangeType={this.onChangeType}
                            onChangeView={this.onChangeView}
                        />
                    </div>
                </div>
            </div>
        )
    }
}

export default connect()(TweetContent)
