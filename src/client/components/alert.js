import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { clearAlert } from '../_actions'

class Alert extends Component {
    constructor(props) {
        super(props)
        props.history.listen(() => {
            this.clearAlertMsg()
        })
    }
    componentWillReceiveProps(nextProps) {
        const { type, msg } = this.props.alert
        const nextAlert = nextProps.alert
        if (msg !== '' && msg !== nextAlert.msg && type !== nextAlert.type) {
            this.clearCurrentTimeout()
        }
    }
    componentWillUnmount() {
        this.clearCurrentTimeout()
    }
    clearCurrentTimeout() {
        const { timeout } = this
        if (timeout) {
            clearTimeout(timeout)
        }
    }
    setCloseTimeout(millis, cb) {
        this.clearCurrentTimeout()
        this.timeout = setTimeout(() => {
            this.clearAlertMsg()
        }, millis)
    }
    clearAlertMsg() {
        const { alert: { msg }, dispatch } = this.props
        if (msg) {
            dispatch(clearAlert())
        }
    }
    handleClose = e => {
        this.setCloseTimeout(1000)
    }
    render() {
        const { type, msg } = this.props.alert
        return (
            <div>
                {msg &&
                    <div>
                        <div className={`alert alert-${type} alert-dismissible fade show`} role="alert">
                        {msg}
                        <button type="button" className="close" data-dismiss="alert" aria-label="Close" onClick={this.handleClose}>
                            <span aria-hidden="true">&times;</span>
                        </button>
                        </div>
                    </div>
                }
            </div>
        )
    }
}

const mapStateToProps = state => ({ alert: state.alert })

export default withRouter(connect(mapStateToProps)(Alert))
