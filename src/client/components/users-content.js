import React, { Component } from 'react'
import { connect } from 'react-redux'
import { TopUsers, Sidebar, Loading, Pagination, paginate } from './'
import { alertError } from '../_actions'
import twitterApi from '../twitter-api'

let mounted = false

const KEY_USER_ORDER = 'setting_user_order'

class UsersContent extends Component {
    state = {
        max_id: 0,
        loading: true,
        hasNext: true,
        order: 'desc',
        users: []
    }
    async componentDidMount() {
        mounted = true
        const { dispatch } = this.props
        let { order } = this.state
        order = localStorage.getItem(KEY_USER_ORDER) || order
        let users = []
        try {
            users = await twitterApi.fetchPopularUsers()
            if (order === 'asc') {
                users = users.reverse()
            }
        } catch (err) {
            dispatch(alertError(err))
        }
        if (mounted) {
            this.setState({ loading: false, users })
        }
    }
    componentWillUnmount() {
        mounted = false
    }
    onChangeOrder = value => {
        this.setState({ order: value, users: this.state.users.reverse() })
        localStorage.setItem(KEY_USER_ORDER, value)
    }
    render() {
        let { page } = this.props
        let{ users, loading, order } = this.state
        const pagination = paginate(page, users, 20, 3)
        users = pagination.items
        delete pagination.items
        return (
            <div className="row mt-2 mb-2">
                {loading
                    ? <div className="col"><Loading /></div>
                    : <div className="col">
                        <Pagination {...pagination} />
                        <TopUsers users={users} />
                        <Pagination {...pagination} />
                    </div>
                    }
                <div className="col-2 sidebar">
                    <div className="sticky-top sticky-offset">
                        <Sidebar
                            order={order}
                            onChangeOrder={this.onChangeOrder}
                            forUsers={true}
                        />
                    </div>
                </div>
            </div>
        )
    }
}

export default connect()(UsersContent)
