import React, { Component } from 'react'

class Sidebar extends Component {
    onChangeDate = ({ target: { value } }) => {
        this.props.onChangeDate(value)
    }
    onChangeOrder = ({ target: { value } }) => {
        this.props.onChangeOrder(value)
    }
    onChangeType = ({ target: { value } }) => {
        this.props.onChangeType(value)
    }
    onChangeView = ({ target: { value } }) => {
        this.props.onChangeView(value)
    }
    render() {
        const { date, order, type, view, forUsers, forUserFeeds } = this.props
        return (
            <div className="sidebar-options">
                <h3>Options</h3>
                {!forUsers &&
                    <div className="input-group mb-5">
                        <div className="input-group-prepend">
                            <label className="input-group-text" htmlFor="byDate">Tweets by: </label>
                        </div>
                        <select className="custom-select" id="byDate" value={date} onChange={this.onChangeDate}>
                            <option value="today">Today</option>
                            <option value="week">This week</option>
                            <option value="month">This month</option>
                            <option value="year">This year</option>
                        </select>
                    </div>
                }
                <div className="input-group mb-5">
                    <div className="input-group-prepend">
                        <label className="input-group-text" htmlFor="order">Sort by: </label>
                    </div>
                    <select className="custom-select" id="order" value={order} onChange={this.onChangeOrder}>
                        <option value="desc">Descending</option>
                        <option value="asc">Ascending</option>
                    </select>
                </div>
                {forUserFeeds &&
                    <div className="input-group mb-5">
                        <div className="input-group-prepend">
                            <label className="input-group-text" htmlFor="type">Sort Type: </label>
                        </div>
                        <select className="custom-select" id="type" value={type} onChange={this.onChangeType}>
                            <option value="recent">Recent</option>
                            <option value="popularity">Popularity</option>
                        </select>
                    </div>
                }
                {!forUsers &&
                    <div className="input-group mb-5">
                        <div className="input-group-prepend">
                            <label className="input-group-text" htmlFor="view">View: </label>
                        </div>
                        <select className="custom-select" id="view" value={view} onChange={this.onChangeView}>
                            <option value="rows">Row</option>
                            <option value="grid">Grid</option>
                        </select>
                    </div>
                }
            </div>
        )
    }
}


export default Sidebar
