import React, { Component } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Link } from 'react-router-dom'
import twitterApi from '../twitter-api'

const Loading = () => (
    <div className="text-center">
      <h4 className="mt-2">
        <FontAwesomeIcon icon="spinner" spin className="mr-1" />
        Loading...
      </h4>
    </div>
)

const Suggestions = ({ loading, results, query, onClickItem }) => {
    return (
        <div className="dropdown-menu" style={(loading || results.length) && query ? { display: 'block' } : {}}>
            {loading
            ? <Loading />
            : results.map(r => (
                <Link className="dropdown-item" key={r.id} to={`/user/${r.screen_name}`} onClick={onClickItem}>
                    <div className="tweet-user-container">
                        <img className="tweet-avatar rounded-circle" src={r.profile_image_url} alt="" />
                        <span className="tweet-fullname">{r.name}</span>
                        <span className="tweet-username"><s>@</s><b>{r.screen_name}</b></span>
                    </div>
                </Link>
            ))
            }
        </div>
    )
}

export default class Search extends Component {
    state = {
        loading: false,
        query: '',
        results: []
    }
    getOptions = async input => {
        try {
            const results = await twitterApi.searchUsers(input)
            this.setState({ loading: false, results })
            return
        } catch (err) {
            this.setState({ loading: false })
            return
        }
    }
    handleChange = async () => {
        this.setState({
            query: this.search.value
        }, async () => {
            const { query } = this.state
            if (query && query.length > 2) {
                const { timeout } = this
                const { loading } = this.state
                if (timeout) {
                    clearTimeout(timeout)
                }
                if (!loading) {
                    this.setState({ loading: true })
                }
                this.timeout = setTimeout(async () => {
                    await this.getOptions(query)
                }, 1000)
            }
        })
    }
    resetSearch = () => {
        this.search.value = ''
        this.setState({         
            query: '',
            results: []
        })
    }
    handleBlur = e => {
        setTimeout(() => this.resetSearch(), 1000)
    }
    handleClickItem = e => {
        this.resetSearch()
    }
    handleSearch = e => {
        e.preventDefault()
        this.resetSearch()
    }
    render() {
        return (
            <div className="dropdown" onBlur={this.handleBlur}>
                <form className="form-inline my-2 my-lg-0" onSubmit={this.handleSearch}>
                    <div className="search input-group input-group-sm border bg-white">
                        <input type="search" className="form-control border-0 mx-1 my-1" name="search" 
                        placeholder="Search by account name" aria-label="Search" autoComplete="off"
                        ref={input => this.search = input} onChange={this.handleChange} />
                        <button className="btn btn-sm btn-light border-0 mx-1 my-1 bg-white">
                            <FontAwesomeIcon icon="search" color="#6c757d" className="my-auto" />
                        </button>
                    </div>
                </form>
                <Suggestions { ...this.state } onClickItem={this.handleClickItem} />
            </div>
        )
    }
}
