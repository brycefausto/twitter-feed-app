import React from 'react'
import PropTypes from 'prop-types'
import Header from './Header'
import Text from './Text'
import Footer from './Footer'
import styles from './styles'
import {cloneDeep} from './utils'

class Modal extends React.Component {
  close () {
    this.context.closeModal()
  }

  render () {
    if (typeof window === "undefined") return null

    let {data, modalIndex} = this.props

    // use retweet as data if its a RT
    if (data.retweeted_status) {
      data = data.retweeted_status
    }

    let media = data.entities.media[modalIndex]

    if (data.extended_entities && data.extended_entities.media) {
      media = data.extended_entities.media[modalIndex]
    }

    const tweetStyle = {
      'backgroundColor': '#ffffff',
      'minHeight': '51px',
      'minWidth': '350px',
      'color': 'white',
      'fontFamily': '"Helvetica Neue", "Helvetica", "Arial", "sans-serif"',
      'fontSize': '14px',
      'lineHeight': '20px',
      'listStyleImage': 'none',
      'listStylePosition': 'outisde',
      'listStyleType': 'none',
      'position': 'relative',
      'textAlign': 'start'
    }

    let contentStyle = cloneDeep(styles.content)
    contentStyle.padding = styles.tweet.padding

    let modalWrap = {
      'position': 'relative',
      'display': 'inline-block',
      'verticalAlign': 'middle',
      'margin': '0 auto',
      'zIndex': 20,
      'marginTop': '5px',
      'borderRadius': '5px',
      'overflow': 'hidden',
    }

    let imgStyle = {
      'width': '100%',
      'margin': '0 auto',
      'display': 'block'
    }

    let imgWrapStyle = {
      'width': '100%',
      'background': 'black'
    }

    const w = media.sizes.large.w;
    const h = media.sizes.large.h;
    const maxSize = 500;

    if (w > 1000) {
      if (h > maxSize) {
        const width = `${(media.sizes.large.w / media.sizes.large.h) * maxSize}px`
        imgStyle.width = width
        modalWrap.width = width
      } 
      else {
        modalWrap.width = `${(media.sizes.large.w / media.sizes.large.h) * maxSize}px`
        imgStyle.height = `${(media.sizes.large.h / media.sizes.large.w) * 1000}px`
      }
    }
    else {
      if (h > maxSize) {
        modalWrap.width = `${(media.sizes.large.w / media.sizes.large.h) * maxSize}px`
        imgStyle.width = `${(media.sizes.large.w / media.sizes.large.h) * maxSize}px`
      }
      else {
        modalWrap.width = `${w}px`
      }
    }

    return (
      <div className="Modal" style={styles.Modal}>
        <span style={{'height': '100%', 'display': 'inline-block', 'verticalAlign': 'middle'}} />
        <div className="ModalClose" style={styles.ModalClose} onClick={this.close.bind(this)} />
        <div className="Modal-wrap" style={modalWrap}>
          <div className="tweet" style={tweetStyle}>
            <div className="media-wrap" style={imgWrapStyle}>
              <img src={media.media_url} style={imgStyle} alt="" />
            </div>
            <div className="content" style={contentStyle}>
              <Header data={data} />
              <Text data={data} />
              <Footer data={data} />
            </div>
          </div>
        </div>
      </div>
    )
  }
}

Modal.contextTypes = {
  'closeModal': PropTypes.func
}

Modal.propTypes = {
  'data': PropTypes.object,
  'active': PropTypes.number
}

Modal.defaultProps = {
  'data': {
    'entities': {},
    'user': {}
  },
  'active': 0
}

export default Modal
