import React, { Component } from 'react'
import { Link, NavLink } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Search from './search'

class Navbar extends Component {
    handleSearch = e => {
        e.preventDefault()
    }
    render() {
        const { isAuth, email } = this.props.user
        return (
            <nav className="navbar navbar-expand-lg navbar-light bg-light sticky-top">
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>

                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <div className="navbar-nav mr-auto">
                        <NavLink className="nav-item nav-link" activeClassName="active" to="/">
                            <FontAwesomeIcon icon="home" color="#007bff" className="mr-1" />
                            Home
                        </NavLink>
                        <NavLink className="nav-item nav-link" activeClassName="active" to="/top_users">
                            <FontAwesomeIcon icon="users" color="#007bff" className="mr-1" />
                            Top Users
                        </NavLink>
                    </div>
                    {isAuth &&                                 
                        <Search />
                    }
                    <div className="navbar-nav pl-5 pr-2">
                        {isAuth ? 
                        (
                            <div className="nav-item dropdown">
                                <a className="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <FontAwesomeIcon icon="user" color="#007bff"/>
                                </a>
                                <div className="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <h6 className="dropdown-header">{email}</h6>
                                    <div className="dropdown-divider"></div>
                                    <NavLink className="dropdown-item" activeClassName="active" to="/account">My Account</NavLink>
                                    <div className="dropdown-divider"></div>
                                    <Link className="dropdown-item" to="/logout">Log out</Link>
                                </div>
                            </div>
                        ) :
                        (
                            <div className="buttons">
                                <Link className="btn btn-outline-primary" to="/login">Log in</Link>
                                <Link className="btn btn-outline-primary" to="/signup">Sign Up</Link>
                            </div>
                        )}
                    </div>
                </div>
            </nav>
        )
    }
}

export default Navbar
