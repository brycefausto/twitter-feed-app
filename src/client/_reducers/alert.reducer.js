import { createReducer } from 'redux-act'
import * as actions from '../_actions/alert.actions'

// alert types for bootstrap
const TYPES = {
    SUCCESS: 'success',
    WARNING: 'warning',
    ERROR: 'danger'
}

const initialState = {
    type: '',
    msg: ''
}

export default createReducer({
    [actions.alertSuccess]: (state, payload) => ({ type: TYPES.SUCCESS, msg: payload }),
    [actions.alertWarning]: (state, payload) => ({ type: TYPES.WARNING, msg: payload }),
    [actions.alertError]: (state, payload) => ({ type: TYPES.ERROR, msg: payload }),
    [actions.clearAlert]: state => (initialState),
}, initialState)
