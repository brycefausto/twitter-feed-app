import { combineReducers } from 'redux'
import alertReducer from './alert.reducer'
import userReducer from './user.reducer'
import { reducer as formReducer } from 'redux-form'

const rootReducer = combineReducers({
    alert: alertReducer,
    user: userReducer,
    form: formReducer
})

export default rootReducer
