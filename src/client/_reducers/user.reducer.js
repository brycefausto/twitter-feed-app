import { createReducer } from 'redux-act'
import * as actions from '../_actions/user.actions'

const initialState = {
    _id: '',
    email: '',
    isAuth: false
}

export default createReducer({
    [actions.setUser]: (state, { _id, email }) => ({ _id: _id, email: email, isAuth: true }),
    [actions.clearUser]: state => (initialState),
}, initialState)
