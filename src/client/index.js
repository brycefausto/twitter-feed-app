import React from 'react'
import ReactDOM from 'react-dom'

import 'bootstrap/dist/css/bootstrap.min.css'
import './index.css'
import 'jquery/dist/jquery.slim.min'
import 'bootstrap/dist/js/bootstrap.bundle.min'

import { library } from '@fortawesome/fontawesome-svg-core'
import { faSpinner, faHome, faUsers, faUser, faSearch } from '@fortawesome/free-solid-svg-icons'

import App from './App'
import { Provider } from 'react-redux'
import store from './_store'

library.add(faSpinner, faHome, faUsers, faUser, faSearch)

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
)
