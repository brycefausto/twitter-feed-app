let hasInternet = true;

export function isOnline() {
    return hasInternet
}

export function setHasInternet(bool) {
    hasInternet = bool
}
